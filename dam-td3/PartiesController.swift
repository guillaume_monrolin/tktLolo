//
//  PartiesController.swift
//  dam-td3
//
//  Created by MONROLIN Guillaume on 28/11/2017.
//  Copyright © 2017 Florian Rimoli. All rights reserved.
//

import UIKit
import SWXMLHash

class PartiesController: UITableViewController {

    
    var events : Array<Event> = Array()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = "http://sfcr.lanoosphere.com/seadata_en.xml"
        self.tableView.register(TableViewCell, forCellReuseIdentifier: "reuseIdentifier")
        URLSession.shared.dataTask(with: NSURL(string: url)! as URL, completionHandler: { (data, response, error) -> Void in
            if error != nil {
                print(error ?? "No Error")
                return
            }
            DispatchQueue.main.async(execute: { () -> Void in
                let xml = SWXMLHash.parse(data!)

                for event in xml["Data"]["Event"].all
                {
                    self.events.append(Event(id: (event.element?.attribute(by: "id")?.text)!, date: (event.element?.attribute(by: "date")?.text)!, name: (event.element?.attribute(by: "name")?.text)!, desc: (event.element?.attribute(by: "desc")?.text)!, flyer: (event.element?.attribute(by: "flyer")?.text)!))
                }
                
                DispatchQueue.main.async(execute: {
                    print("execute")
                    self.tableView.reloadData()
                })
            })
            
        }).resume()
        
        

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.events.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! TableViewCell

        let currentEvent = events[indexPath.row]
        URLSession.shared.dataTask(with: NSURL(string: currentEvent.flyer)! as URL, completionHandler: { (data, response, error) -> Void in
            if error != nil {
                print(error ?? "No Error")
                return
            }
            DispatchQueue.main.async(execute: { () -> Void in
                cell.icon.image = UIImage(data: data!)!
            })
            
        }).resume()
        
        cell.date.text = currentEvent.date
        cell.time.text = currentEvent.date
        cell.title.text = currentEvent.name

        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
}
