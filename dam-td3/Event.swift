//
//  Event.swift
//  dam-td3
//
//  Created by MONROLIN Guillaume on 28/11/2017.
//  Copyright © 2017 Florian Rimoli. All rights reserved.
//

import UIKit

class Event {
    var id : String
    var date : String
    var name : String
    var desc : String
    var flyer : String
    
    init(id: String, date: String, name: String, desc: String, flyer: String) {
        self.id = id
        self.date = date
        self.name = name
        self.desc = desc
        self.flyer = flyer
    }
}
