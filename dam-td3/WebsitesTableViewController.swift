//
//  WebsitesTableViewController.swift
//  dam-td3
//
//  Created by Florian Rimoli on 28/11/2017.
//  Copyright © 2017 Florian Rimoli. All rights reserved.
//

import UIKit

struct WebSite {
    var name: String
    var urlString: String
}

class WebSiteCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel:UILabel!
    @IBOutlet weak var urlLabel: UILabel!
    @IBOutlet weak var iconView: UIImageView!
    
    func setImageUrl(urlStr: String) {
        URLSession.shared.dataTask(with: NSURL(string: urlStr)! as URL, completionHandler: { (data, response, error) -> Void in
            if error != nil {
                print(error ?? "No Error")
                return
            }
            DispatchQueue.main.async(execute: { () -> Void in
                let image = UIImage(data: data!)
                self.iconView.image = image
            })
            
        }).resume()
    }
}

class WebsitesTableViewController: UITableViewController {
    
    var websites = [
        WebSite(name: "Apple", urlString: "http://apple.com"),
        WebSite(name: "Google", urlString: "http://google.com"),
        WebSite(name: "Amazon", urlString: "http://amazon.com"),
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Websites"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.websites.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "websiteCell", for: indexPath) as! WebSiteCell
        cell.nameLabel.text = self.websites[indexPath.row].name
        cell.urlLabel.text = self.websites[indexPath.row].urlString
        cell.setImageUrl(urlStr: "\(self.websites[indexPath.row].urlString)/favicon.ico")
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let website = self.websites[indexPath.row]
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "WEBVIEWCONTROLLER") as! WebViewController
        controller.website = website
        self.navigationController?.pushViewController(controller, animated: true)
    }
    


}
