//
//  WebViewController.swift
//  dam-td3
//
//  Created by Florian Rimoli on 28/11/2017.
//  Copyright © 2017 Florian Rimoli. All rights reserved.
//

import UIKit

class WebViewController: UIViewController {
    
    var website: WebSite!
    @IBOutlet weak var webView: UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.title = self.website.name
        if let url = URL(string: self.website.urlString) {
            self.webView.loadRequest(URLRequest(url: url))
        }
        else {
            print("not a valid URL")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
