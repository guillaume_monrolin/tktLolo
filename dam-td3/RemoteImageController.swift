//
//  RemoteImageController.swift
//  dam-td3
//
//  Created by Florian Rimoli on 28/11/2017.
//  Copyright © 2017 Florian Rimoli. All rights reserved.
//

import UIKit

class RemoteImageController: UIViewController {
    
    var imageView: UIImageView?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func addImage() {
        self.imageView = UIImageView()
        self.imageView?.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width - 60, height: 250)
        self.imageView?.center = self.view.center
        self.imageView?.backgroundColor = UIColor.orange
        self.view.addSubview(self.imageView!)
    }
    
    @IBAction func localImage() {
        self.imageView?.contentMode = .scaleAspectFit
        self.imageView?.image = UIImage(named: "local_image")
    }

    @IBAction func remoteImage() {
        let urlString = "http://angoangari.com/data/imagegallery/0df7ff40-368d-c402-fcd2-0203fbfb07c0/ac264638-0ac6-d611-d7ba-73ed9fd266b7.jpg"
        
        URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data, response, error) -> Void in
            if error != nil {
                print(error ?? "No Error")
                return
            }
            DispatchQueue.main.async(execute: { () -> Void in
                let image = UIImage(data: data!)
                self.imageView?.image = image
            })
            
        }).resume()
    }
}
